# Giga profile

Deploy `giga` profile based on the [sxcm giga profile](https://gitlab.com/sxcm/docs/-/raw/main/src/profiles/install-config-giga.yml) that is part of the [sxcm default profiles](../../3-profiles).

## Description

| Field          | Content                                                                                                                                           |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Name**       | **giga**                                                                                                                                          |
| **Scope**      | shared                                                                                                                                            |
| **cost**       | ***2,67 € / hour***                                                                                                                               |
| **Datacenter** | _AWS London_                                                                                                                                      |
| **Topology**   | **3** master + **3** worker                                                                                                                       |
| **Master**     | **8**Vcpu, **16**Go RAM, **200**Go gp2  200iop                                                                                                    |
| **Worker**     | **8**Vcpu,  **64**Go RAM, **200**Go gp2  200iop                                                                                                   |
| **service**    | `argocd`  <br/> `pipeline`  <br/> `istio`  <br/> `quaysec`  <br/> `machine`  <br/> `workspaces`  <br/> `couchbase`  <br/> `sso`  <br/> `vault`  <br/> `knative`  <br/> `logging`  <br/> `acm`  <br/> `quay`  <br/> `ocs`  <br/> `metering`  <br/> `3scale` |

## Create a cluster

```bash
# Create this cluster to the cluster stack (based on this profile)
sxcm create mycluster giga
```
