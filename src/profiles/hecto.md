# Hecto profile

Deploy `hecto` profile based on the [sxcm hecto profile](https://gitlab.com/sxcm/docs/-/raw/main/src/profiles/install-config-hecto.yml) that is part of the [sxcm default profiles](../../3-profiles).

## Description

| Field          | Content                                                                                                                                           |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Name**       | **hecto**                                                                                                                                         |
| **Scope**      | shared                                                                                                                                            |
| **cost**       | ***1,33 € / hour***                                                                                                                               |
| **Datacenter** | _AWS Paris_                                                                                                                                       |
| **Topology**   | **3** master + **6** worker                                                                                                                       |
| **Master**     | **4**Vcpu, **16**Go RAM, **120**Go io1  120iop                                                                                                    |
| **Worker**     | **4**Vcpu,  **16**Go RAM, **200**Go io1  200iop                                                                                                   |
| **service**    | `argocd`  <br/> `pipeline`  <br/> `istio`  <br/> `quaysec`  <br/> `machine`  <br/> `workspaces`  <br/> `couchbase`  <br/> `sso`  <br/> `vault`  <br/> `knative`  <br/> `logging`  <br/> `acm`  <br/> `quay`  <br/> `ocs`  <br/> `metering`  <br/> `3scale` |

## Create a cluster

```bash
# Create this cluster to the cluster stack (based on this profile)
sxcm create mycluster hecto
```
