# Demo Workspace

The **workspace** [demo](../../5-demos) use the [workspace cluster resource](../../resources/workspace) to deploy multiple Workspace cluster using the workspace operator with several example applications working with them.

This demo is based on the [cluster-workspace helm chart](https://helm-repository.readthedocs.io/en/latest/charts/cluster-workspace) defined in [startx helm-repository](https://helm-repository.readthedocs.io).

## Description

| Field            | Content                                                 |
| ---------------- | ------------------------------------------------------- |
| **Name**         | **STARTX demo workspace**                               |
| **tags**         | `startx` `demo` `ide` `workspace`                       |
| **provider**     | STARTX                                                  |
| **requirements** | [workspace cluster resource](../../resources/workspace) |
| **format**       | openshift template with argoCD application              |
| **content**      | **6** Applications + **1** AppProject                   |
| **namespace**    | demo-workspace                                          |

## Add to a cluster

```bash
# Associate this demo to the cluster mycluster (runable demo)
sxcm demo associate workspace mycluster
# Enable this demo to the cluster mycluster (running demo)
sxcm demo enable workspace mycluster
```

## Remove from a cluster

```bash
# Dissociate this demo from the cluster mycluster (removable demo)
sxcm demo dissociate workspace mycluster
# Disable this demo from the cluster mycluster (removed demo)
sxcm demo disable workspace mycluster
```

## Get demo detail

```bash
# Read information about the demo version installed into your host (local)
sxcm demo info workspace
```

## More on demo

You should read the [demo management](../../5-demos) documentation section to learn the command
meaning and how to addapt your own demo for a personalized cluster configuration.
