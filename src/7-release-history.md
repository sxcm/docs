# SXCM history


## version 12.0.x (chastang)

_The objectif of this release is to stabilize the full repository content and offer a dev (regular ehancements) to stable (regular secfix) content for a global coverage of environements requirements._

## version 11.0.x (chazal)

This version is designed and stabilized for the [Openshift](https://docs.openshift.com) **[4.11 release](https://docs.openshift.com/container-platform/4.11)**.

## version 10.0.x (chauvignac)

This version is designed and stabilized for the [Openshift](https://docs.openshift.com) **[4.10 release](https://docs.openshift.com/container-platform/4.10)**.

## version 9.x.x (claux)

This version is designed and stabilized for the [Openshift](https://docs.openshift.com) **[4.9 release](https://docs.openshift.com/container-platform/4.9)**.

The objectif of this version is :

- Stable support for cluster service running under an Openshift 4.9.x cluster
- Stable helm-repository catalog
- Stable artfacthub and readthedocs publication
- move to gitlab-ci backend

| Release                                                         | Date     | Description                         |
| --------------------------------------------------------------- | -------- | ----------------------------------- |
| [9.8.3](https://github.com/startxfr/sxcm/releases/tag/v9.8.3)   | 21-12-12 | Generate documentation form gitlab with autonomous lifecycle |
| [9.8.1](https://github.com/startxfr/sxcm/releases/tag/v9.8.1)   | 21-11-20 | Stable for openshift 4.9.8 release |

## version 8.x.x (charbonnel)

This version is designed and stabilized for the [Openshift](https://docs.openshift.com) **[4.8 release](https://docs.openshift.com/container-platform/4.8)**.

The objectif of this version is :

- Stable support for cluster service running under an Openshift 4.8.x cluster
- Stable helm-repository catalog

| Release                                                         | Date     | Description                         |
| --------------------------------------------------------------- | -------- | ----------------------------------- |
| [8.20.7](https://github.com/startxfr/sxcm/releases/tag/v8.20.7) | 21-11-20 | Stable for openshift 4.8.21 release |
| [8.20.5](https://github.com/startxfr/sxcm/releases/tag/v8.20.5) | 21-11-14 | Revamped CI pipeline                | 
| [8.20.3](https://github.com/startxfr/sxcm/releases/tag/v8.20.3) | 21-11-11 | Stable for openshift 4.8.20 release |
| [8.13.1](https://github.com/startxfr/sxcm/releases/tag/v8.13.1) | 21-10-07 | Stable for openshift 4.8.13 release |
| 8.3.3                                                           | 21-08-03 | Stable for openshift 8.3.3 release  |

## version 7.22.x (chaumeil)

This version is designed and stabilized for the [Openshift](https://docs.openshift.com) **[4.7 release](https://docs.openshift.com/container-platform/4.7)**.

The objectif of this version is :

- Stable support for cluster service running under an Openshift 4.7.x cluster
- Stable helm-repository catalog

| Release                                                         | Date     | Description                         |
| --------------------------------------------------------------- | -------- | ----------------------------------- |
| [7.22.3](https://github.com/startxfr/sxcm/releases/tag/v7.22.3) | 21-08-03 | Stable for openshift 4.7.22 release |

## version 6.x.x (chassagne)

This version covert both [Openshift](https://docs.openshift.com) **[4.5](https://docs.openshift.com/container-platform/4.5)**, **[4.6](https://docs.openshift.com/container-platform/4.6)** and **[4.7](https://docs.openshift.com/container-platform/4.7)** release.

The objectif of this version is :

- Offer a stable support for cluster service running under an Openshift 4.5.x, 4.6.x or 4.7.x cluster
- Add more content to the helm repository catalog
- Finish to add demo in the example-catalog
- Move documentation to readthedocs infrastructure

| Release                                                       | Date     | Description                        |
| ------------------------------------------------------------- | -------- | ---------------------------------- |
| [6.1.1](https://github.com/startxfr/sxcm/releases/tag/v6.1.1) | 21-08-03 | Stable for openshift 4.6.1 release |

## version 0.1.x (chauzu)

The objectif of this release is to get a release for managing OCP 4.7 cluster under AWS in a gitOps way of working

| Release                                                        | Date     | Description                                                                                 |
| -------------------------------------------------------------- | -------- | ------------------------------------------------------------------------------------------- |
| 0.1.7                                                          | planned  | improve code quality and reduct application debt                                            |
| [0.1.6](https://github.com/startxfr/sxcm/releases/tag/v0.1.6)  | 21-08-03 | Move to Openshift 4.7.22 install                                                            |
| [0.1.5](https://github.com/startxfr/sxcm/releases/tag/v0.1.5)  | 21-06-04 | Move to Openshift 4.7.13 install and add demo and history sub-command                       |
| [0.1.4](https://github.com/startxfr/sxcm/releases/tag/v0.1.4)  | 21-05-16 | Full review of the documentation for a stable structure and exaustive content               |
| [0.1.3](https://github.com/startxfr/sxcm/releases/tag/v0.1.3)  | 21-05-14 | Stable version of the resource parameters and cluster resource management                   |
| [0.1.2](https://github.com/startxfr/sxcm/releases/tag/v0.1.2)  | 21-05-13 | Full review of the installer program for a stable support in major Redhat like distribution |
| [0.1.21](https://github.com/startxfr/sxcm/releases/tag/v0.1.1) | 21-05-12 | Enable gitOps experience (cluster states versionned) and cluster resource management        |
| [0.1.0](https://github.com/startxfr/sxcm/releases/tag/v0.1.0)  | 21-05-11 | Stable for openshift 4.7.5 release                                                          |

## version 0.0.x (champeaux)

The objectif of this release is to create the repository structure.

| Release                                                       | Date     | Description                    |
| ------------------------------------------------------------- | -------- | ------------------------------ |
| [0.0.4](https://github.com/startxfr/sxcm/releases/tag/v0.0.4) | 21-04-03 | release sxcm for openshift 4.6 |
| 0.0.3                                                         | 21-04-03 | stable cli basic function      |
| [0.0.2](https://github.com/startxfr/sxcm/releases/tag/v0.0.2) | 21-04-03 | adding the installer           |
| [0.0.1](https://github.com/startxfr/sxcm/releases/tag/v0.0.1) | 21-04-03 | Initial commit                 |

