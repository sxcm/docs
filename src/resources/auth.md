# Configure authentification

The **auth** [cluster resource](../../4-cluster-resources) configure the cluster **openshift authentification** backend for user and group authentification.

This cluster resource is based on the [cluster-auth helm chart](https://helm-repository.readthedocs.io/en/latest/charts/cluster-auth) defined in [startx helm-repository](https://helm-repository.readthedocs.io).

## Description

| Field         | Content                                    |
| ------------- | ------------------------------------------ |
| **Name**      | **STARTX cluster Auth**                    |
| **tags**      | `startx` `cluster` `config` `admin` `auth` |
| **provider**  | STARTX                                     |
| **format**    | openshift template                         |
| **content**   | 1 ArgoCD application                       |
| **namespace** | openshift-config                           |
| **operator**  | _none_                                     |

## Add to a cluster

```bash
# Associate this cluster resource to the cluster mycluster (runable resource)
sxcm resource associate auth mycluster
# Enable this cluster resource to the cluster mycluster (running resource)
sxcm resource enable auth mycluster
```

## Remove from a cluster

```bash
# Dissociate this cluster resource from the cluster mycluster (removable resource)
sxcm resource dissociate auth mycluster
# Disable this cluster resource from the cluster mycluster (removed resource)
sxcm resource disable auth mycluster
```

## Get cluster resource detail

```bash
# Read information about the cluster resource version installed into your host (local)
sxcm resource info auth
```

## More on resource

You should read the [cluster resource management](../../4-cluster-resources) documentation section to learn the command
meaning and how to create your own cluster resource service to deliver personalized cluster configuration.
